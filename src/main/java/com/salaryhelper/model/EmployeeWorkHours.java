package com.salaryhelper.model;

//工时
public class EmployeeWorkHours {
     String employeeName;
     int days3to5H; // 3-5H天数
     int daysGT5H; // >5H天数
     int daysGT11H; // >11H天数
     float weekdaysHours; //工作日工时
     float weekdaysOvertimeHours; //工作日加班工时
     float weekendOvertimeHours; //休息日加班工时
     float holidayHours;  //法定休息日正常工时
     float holidayOvertimeHours; //法定休息日加班工时
     float paidLeaveHours; //带薪假工时
     float sickLeaveHours; //病假工时
     Boolean isFullAttendance; //是否全勤

     public EmployeeWorkHours() {
     }

     public String getEmployeeName() {
          return employeeName;
     }

     public void setEmployeeName(String employeeName) {
          this.employeeName = employeeName;
     }

     public void setDays3to5H(int days3to5H) {
          this.days3to5H = days3to5H;
     }

     public void setDaysGT5H(int daysGT5H) {
          this.daysGT5H = daysGT5H;
     }

     public void setDaysGT11H(int daysGT11H) {
          this.daysGT11H = daysGT11H;
     }

     public void setWeekdaysHours(float weekdaysHours) {
          this.weekdaysHours = weekdaysHours;
     }

     public void setWeekdaysOvertimeHours(float weekdaysOvertimeHours) {
          this.weekdaysOvertimeHours = weekdaysOvertimeHours;
     }

     public void setWeekendOvertimeHours(float weekendOvertimeHours) {
          this.weekendOvertimeHours = weekendOvertimeHours;
     }

     public void setHolidayHours(float holidayHours) {
          this.holidayHours = holidayHours;
     }

     public void setHolidayOvertimeHours(float holidayOvertimeHours) {
          this.holidayOvertimeHours = holidayOvertimeHours;
     }

     public void setPaidLeaveHours(float paidLeaveHours) {
          this.paidLeaveHours = paidLeaveHours;
     }

     public void setSickLeaveHours(float sickLeaveHours) {
          this.sickLeaveHours = sickLeaveHours;
     }

     public void setFullAttendance(Boolean fullAttendance) {
          isFullAttendance = fullAttendance;
     }

     @Override
     public String toString() {
          return "EmployeeWorkHours{" +
                  "employeeName='" + employeeName + '\'' +
                  ", days3to5H=" + days3to5H +
                  ", daysGT5H=" + daysGT5H +
                  ", daysGT11H=" + daysGT11H +
                  ", weekdaysHours=" + weekdaysHours +
                  ", weekdaysOvertimeHours=" + weekdaysOvertimeHours +
                  ", weekendOvertimeHours=" + weekendOvertimeHours +
                  ", holidayHours=" + holidayHours +
                  ", holidayOvertimeHours=" + holidayOvertimeHours +
                  ", paidLeaveHours=" + paidLeaveHours +
                  ", sickLeaveHours=" + sickLeaveHours +
                  ", isFullAttendance=" + isFullAttendance +
                  '}';
     }
}
