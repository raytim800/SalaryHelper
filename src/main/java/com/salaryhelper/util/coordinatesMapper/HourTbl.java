package com.salaryhelper.util.coordinatesMapper;


public class HourTbl {
    public static int employeeName=0;
    public static int days3to5H=1;
    public static int daysGT5H=2;
    public static int daysGT11H=3;
    public static int weekdaysHours=4;
    public static int weekdaysOvertimeHours=5;
    public static int weekendOvertimeHours=6;
    public static int holidayHours=7;
    public static int holidayOvertimeHours=8;
    public static int paidLeaveHours=9;
    public static int sickLeaveHours=10;
    public static int fullAttendance=11;
}
